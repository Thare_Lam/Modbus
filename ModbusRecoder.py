import os
import time


class Recoder:

    def __init__(self):
        pass

    @staticmethod
    def writerecoder(ip, data, flag):
        path = 'recoder/'
        if not os.path.exists(path):
            os.mkdir(path)
        w = open(path + ip + '.txt', 'a')
        timeformat = '%Y-%m-%d %X'
        t = time.strftime(timeformat, time.localtime())
        w.write(t + ' ' + flag + ' ' + data + '\n')
        w.close()