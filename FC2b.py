# Read Device Identification
import binascii


class DevInfo:
    def __init__(self, objid, content):
        self.objid = objid
        self.content = content
        self.length = self.getlength()

    def getlength(self):
        length = hex(len(self.content))[2:]
        return (2 - len(length)) * '0' + length

    def getobj(self):
        return self.objid + self.length + binascii.b2a_hex(self.content)


def fun(dp):
    if dp.Length != '0005':
        print '1'
        dp.exception('03')
        return
    mei = dp.Data[0:2]
    readdevid = int(dp.Data[2:4], 16)
    objid = int(dp.Data[4:6], 16)
    if mei != '0d' and mei != '0e':
        dp.exception('03')
        print '2'
        return
    if mei == '0d':
        dp.exceotion('04')
        return
    if readdevid < 1 or readdevid > 4:
        print '3'
        dp.exception('03')
        return
    info = []
    info.append(DevInfo('00', 'Schneider Electric  '))
    info.append(DevInfo('01', 'BMX P34 2020'))
    info.append(DevInfo('02', 'v2.2'))
    if readdevid == 4:
        if objid > 2:
            dp.exception('02')
        else:
            response(dp, '01', info[objid].getobj())
    else:
        if objid == 0 or objid > 2:
            response(dp, '03', info[0].getobj() + info[1].getobj() + info[2].getobj())
        elif objid == 1:
            response(dp, '02', info[1].getobj() + info[2].getobj())
        else:
            response(dp, '01', info[2].getobj())
    return


def response(dp, numofobj, infolist):
    dp.response(dp.Data[0:4] + '810000' + numofobj + infolist)



