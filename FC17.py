# Read/Write Multiple Registers


def fun(dp):
    if int(dp.Length, 16) < 12:
        print '3'
        dp.exception('03')
        return
    readstart = int(dp.Data[0:4], 16)
    readcount = int(dp.Data[4:8], 16)
    writestart = int(dp.Data[8:12], 16)
    writecount = int(dp.Data[12:16], 16)
    writebytecount = int(dp.Data[16:18], 16)
    writedatahex = dp.Data[18:]
    if writecount * 2 != writebytecount or len(writedatahex) < writebytecount * 2:
        dp.exception('03')
        return
    if readcount < 1 or readcount > 0x07d or writecount < 1 or writecount > 0x079:
        print '2'
        dp.exception('03')
        return
    if readstart + readcount > 0x07d or writestart + writecount > 0x07d:
        dp.exception('02')
        return
    writedatahex = writedatahex[0:writebytecount*2]
    r = open(dp.file_hregister, 'rb')
    try:
        line = r.readline()
        writedata = line[0:writestart*16] + dp.decitobin(len(writedatahex)*4, int(writedatahex, 16))\
                       + line[(writestart+writecount)*16:]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    w = open(dp.file_hregister, 'wb')
    try:
        assert len(writedata) == 0x7d0
        w.write(writedata)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        w.close()
    r = open(dp.file_hregister, 'rb')
    try:
        readdata = r.read()[readstart*16:(readstart+readcount)*16]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    dp.response(dp.decitohex(2, len(readdata)/8) + dp.decitohex(len(readdata)/4, int(readdata, 2)))