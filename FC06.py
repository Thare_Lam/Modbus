# Write Single Register


def fun(dp):
    if dp.Length != '0006':    # Length is not 6
        dp.exception('03')
        return
    num = int(dp.Data[0:4], 16)
    value = dp.Data[4:8]
    if num >= 0x7d:   # Number isn't correct
        dp.exception('02')
        return
    r = open(dp.file_hregister, 'rb')
    try:
        registerdata = r.readline()
        registerdata = registerdata[0:num*16] + dp.decitobin(16, int(value, 16)) + registerdata[(num+1)*16:]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    w = open(dp.file_hregister, 'wb')
    try:
        assert len(registerdata) == 0x7d0
        w.write(registerdata)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        w.close()
    dp.response('', echo=True)