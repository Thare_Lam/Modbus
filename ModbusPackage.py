# !/usr/bin/python
# version 2.7
# Author - Thare
# https://github.com/Thare-Lam/Modbus.git
# 2016-03-12


import binascii
from ModbusRecoder import Recoder as MR


class DataPackage:

    file_coil = 'data_coil'     # coil data
    file_discrete = 'data_discrete'     # discrete data
    file_hregister = 'data_hregister'   # hold register data
    file_iregister = 'data_iregister'   # input register data
    file_deviceinfo = 'info_device'     # device info

    def __init__(self):
        DataPackage.__init__()

    def __init__(self, ip, client, original, ti, pi, length, ui, fc, data):
        self.IP = ip
        self.Client = client        # Socket Client
        self.Original = original    # Original Receive Data
        self.TI = ti                # Transaction Identifier
        self.PI = pi                # Protocol Identifier
        self.Length = length        # Length
        self.UI = ui                # Unit Identifier
        self.FC = fc                # Function Code
        self.Data = data            # Data after Function Code

    def send(self, strdata):
        hexdata = binascii.a2b_hex(strdata)
        self.Client.sendall(hexdata)
        MR.writerecoder(self.IP, strdata, 'Send')

    def response(self, addata, echo=False):
        """
        Note: normal response
        :param addata: additional hex data after function code
        :param echo: whether echo the receive data(default False)
        """
        if echo:
            self.send(self.Original)
        else:
            data = self.TI + self.PI + self.decitohex(4, len(addata)/2+2) + self.UI + self.FC + addata
            self.send(data)

    def exception(self, ec, other=''):
        """
        Note: occur exception
        :param ec: exception code
        :param other: additional data
        """
        if int(self.FC, 16) <= 0x7f:
            efc = self.decitohex(2, int(self.FC, 16) + 128)
        elif int(self.FC, 16) == 0x90:
            efc = 'c0'
        else:
            efc = self.FC
        exceptiondata = self.TI + self.PI + self.decitohex(4, 3 + len(other)/2) + self.UI \
                        + efc + ec + other
        self.send(exceptiondata)

    def decitohex(self, hexlength, decivalue):  # decimal to hex
        """
        Note:
        :param hexlength: length of hex value
        :param decivalue: decimal value
        :return: hex value without '0x'
        """
        hexvalue = hex(decivalue)[2:].replace('L', '')  # remove '0x' and 'L'
        return (hexlength - len(hexvalue)) * '0' + hexvalue

    def decitobin(self, binlength, decivalue):  # decimal to binary
        """
        Note:
        :param binlength: length of binary value
        :param decivalue: decimal value
        :return: binary value without '0b'
        """
        binvalue = bin(decivalue)[2:]   # remove '0b'
        return (binlength - len(binvalue)) * '0' + binvalue






