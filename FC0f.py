# Write Multiple Coil


def fun(dp):
    if int(dp.Length, 16) < 8:    # Length is not enough
        dp.exception('03')
        return
    start = int(dp.Data[0:4], 16)
    coilcount = int(dp.Data[4:8], 16)
    bytecount = int(dp.Data[8:10], 16)
    writedatahex = dp.Data[10:]
    if (coilcount % 8 == 0 and coilcount / 8 != bytecount) or \
            (coilcount % 8 != 0 and coilcount / 8 + 1 != bytecount) or \
            (len(writedatahex) < bytecount * 2):
        dp.exception('03')
        return
    if coilcount < 1 or coilcount > 0x07d0:   # Count isn't correct
        dp.exception('03')
        return
    if start + coilcount > 0x07d0:   # Number isn't correct
        dp.exception('02')
        return
    writedatahex = writedatahex[0:bytecount*2]
    writedatabin = dp.decitobin(8 * bytecount, int(writedatahex, 16))
    writedataopt = ''
    for i in range(0, len(writedatabin), 8):
        writedataopt += writedatabin[i:i+8][::-1]
    r = open(dp.file_coil, 'rb')
    try:
        coildata = r.readline()
        coildata = coildata[0:start] + writedataopt[0:coilcount] + coildata[start+coilcount:]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    w = open(dp.file_coil, 'wb')
    try:
        assert len(coildata) == 0x7d0
        w.write(coildata)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        w.close()
    dp.response(dp.Data[0:8])
