# Mask Write Register


def fun(dp):
    if int(dp.Length, 16) != 8:
        dp.exception('03')
    num = int(dp.Data[0:4], 16)
    andmask = int(dp.Data[4:8], 16)
    ormask = int(dp.Data[8:12], 16)
    if num >= 0x7d:
        dp.exception('03')
        return
    r = open(dp.file_hregister, 'rb')
    try:
        line = r.readline()
        objregister = int(line[num*16:(num+1)*16], 16)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    result = (objregister & andmask) | (ormask & ~andmask)
    line = line[0:num*16] + dp.decitobin(16, result) + line[(num+1)*16:]
    w = open(dp.file_hregister, 'wb')
    try:
        assert len(line) == 0x7d0
        w.write(line)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        w.close()
    dp.response('', echo=True)
