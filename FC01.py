# Read Coils


def fun(dp):
    if dp.Length != '0006':    # Length is not 6
        dp.exception('03')
        return
    start = int(dp.Data[0:4], 16)   # ReferenceNumber
    count = int(dp.Data[4:8], 16)   # BitCount
    if count < 1 or count > 0x07d0:   # Count isn't correct
        dp.exception('03')
        return
    if start + count > 0x07d0:   # Number isn't correct
        dp.exception('02')
        return
    read = open(dp.file_coil, 'rb')
    try:
        datastream = read.readline()[start:start + count]
        if count % 8 != 0:
            datastream += (8 - count % 8) * '0'
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        read.close()
    dp.response(dp.decitohex(2, len(datastream)/8) + handledatastream(dp, datastream))


def handledatastream(dp, datastream):
    datastreamopt = ''
    for i in range(0, len(datastream), 8):
        datastreamopt += datastream[i:i+8][::-1]    # reverse by every 8 bits
    return dp.decitohex(len(datastream)/4, int(datastreamopt, 2))


