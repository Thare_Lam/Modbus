# !/usr/bin/python
# version 2.7
# Author - Thare
# https://github.com/Thare-Lam/Modbus.git
# 2016-02-27
"""
                            ***Modbus Protocol***
Data Access:                                                 Function Code
    - Bit Access:                                             Code     Hex
        - Physical Discrete        Read Discrete Inputs         02      02
                Inputs
        ------------------------------------------------------------------
        - Internal Bits            Read Coils                   01      01
                Or                 Write Single Coils           05      05
          Physical Coils           Write Multiple Coils         15      0F
    **********************************************************************
    - 16 Bits Access
        - Physical Input           Read Input Register          04      04
            Registers
        ------------------------------------------------------------------
        - Internal Registers       Read Holding Register        03      03
                Or                 Write Single Register        06      06
          Physical Output          Write Multiple Registers     16      10
             Registers             Read/Write Multiple Register 23      17
                                   Mask Write Register          22      16
                                   Read FIFO Queue              24      18
    **********************************************************************
    - File Record Access
                                   Read File Record             20      14
                                   Write File Record            21      15
==========================================================================
Other:
                                   Read Device Identification   43      2b
==========================================================================
Serial Line only(Don't simulate):
                                   07,08,0b,0c,11
==========================================================================
==========================================================================
Exception Code:
    - 01: Function code isn't supported.
    - 02: Address isn't OK.
    - 03: Count isn't correct.
    - 04: Operation error.
"""

import os
import socket
import threading

from ModbusPackage import *
import FC01
import FC02
import FC03
import FC04
import FC05
import FC06
import FC0f
import FC10
import FC16
import FC17
import FC2b

BUFFERSIZE = 1024
address = ('0.0.0.0', 502)
backlog = 10


def optrequest(client, ip):
    while 1:
        recv = binascii.b2a_hex(client.recv(BUFFERSIZE))
        if len(recv) == 0:
            continue
        MR.writerecoder(ip, recv, 'Recv')
        if len(recv)/2 < 8:
            continue
        dp = DataPackage(ip, client, recv, recv[0:4], recv[4:8], recv[8:12], recv[12:14],
                         recv[14:16], recv[16:16+int(recv[8:12], 16)*2-4])
        if int(dp.Length, 16) > (len(recv)/2-6):
            continue
        if int(dp.Length, 16) < (len(recv)/2-6):
            dp.Original = dp.Original[0:int(dp.Length, 16) * 2 + 8]

        try:
            eval('FC' + dp.FC).fun(dp)
        except:
            dp.exception('01')


if __name__ == '__main__':
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(address)
    sock.listen(backlog)
    while 1:
        client, address = sock.accept()
        thread = threading.Thread(target=optrequest, args=(client, address[0]))
        thread.start()










