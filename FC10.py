# Write Multiple Registers


def fun(dp):
    if int(dp.Length, 16) < 8:    # Length is not enough
        dp.exception('03')
        return
    start = int(dp.Data[0:4], 16)
    registercount = int(dp.Data[4:8], 16)
    bytecount = int(dp.Data[8:10], 16)
    writedatahex = dp.Data[10:]
    if registercount * 2 != bytecount or len(writedatahex) < bytecount * 2:
        dp.exception('03')
        return
    if registercount < 1 or registercount > 0x07b:   # Count isn't correct
        dp.exception('03')
        return
    if start + registercount > 0x07d:   # Number isn't correct
        dp.exception('02')
        return
    writedatahex = writedatahex[0:bytecount*2]
    r = open(dp.file_hregister, 'rb')
    try:
        registerdata = r.readline()
        registerdata = registerdata[0:start*16] + dp.decitobin(len(writedatahex)*4, int(writedatahex, 16))\
                       + registerdata[(start+registercount)*16:]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    w = open(dp.file_hregister, 'wb')
    try:
        assert len(registerdata) == 0x7d0
        w.write(registerdata)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        w.close()
    dp.response(dp.Data[0:8])
