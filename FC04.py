# Read Input Registers


def fun(dp):
    if dp.Length != '0006':    # Length is not 6
        dp.exception('03')
        return
    start = int(dp.Data[0:4], 16)   # ReferenceNumber
    count = int(dp.Data[4:8], 16)   # BitCount
    if count < 1 or count > 0x07d:   # Count isn't correct
        dp.exception('03')
        return
    if start + count > 0x07d:   # Number isn't correct
        dp.exception('02')
        return
    read = open(dp.file_iregister, 'rb')
    try:
        datastream = read.readline()[start*16:(start+count)*16]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        read.close()
    dp.response(dp.decitohex(2, len(datastream)/8) + handledatastream(dp, datastream))


def handledatastream(dp, datastream):
    return dp.decitohex(len(datastream)/4, int(datastream, 2))
