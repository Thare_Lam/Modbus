# Write Single Coil


def fun(dp):
    if dp.Length != '0006':    # Length is not 6
        dp.exception('03')
        return
    num = int(dp.Data[0:4], 16)
    value = dp.Data[4:8]
    if num >= 0x07d0:   # Number isn't correct
        dp.exception('02')
        return
    if value != 'ff00' and value != '0000':      # Illegal data value
        dp.exception('03')
        return
    r = open(dp.file_coil, 'rb')
    try:
        coildata = r.readline()
        if value == 'ff00':
            coildata = coildata[0:num] + '1' + coildata[num+1:]
        else:
            coildata = coildata[0:num] + '0' + coildata[num+1:]
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        r.close()
    w = open(dp.file_coil, 'wb')
    try:
        assert len(coildata) == 0x7d0
        w.write(coildata)
    except Exception, e:
        print e
        dp.exception('04')
        return
    finally:
        w.close()
    dp.response('', echo=True)
